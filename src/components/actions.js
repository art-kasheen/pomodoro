import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { start, stop, reset } from '../ac'

const Actions = ({ timer, start, stop, reset }) => {
  const onTimerStart = () => {
    start()
  }

  const onTimerStop = () => {
    stop()
  }

  const onTimerReset = () => {
    reset()
  }

  return (
    <div className="app-actions">
      <button
        className="app-button"
        onClick={timer.isActive ? onTimerStop : onTimerStart}
      >
        {timer.isActive ? 'Pause' : 'Start'}
      </button>
      <button className="app-button" onClick={onTimerReset}>
        Reset
      </button>
    </div>
  )
}

Actions.propsTypes = {
  timer: PropTypes.object.isRequired,
  start: PropTypes.func.isRequired,
  stop: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired
}

export default connect(
  (state) => ({ timer: state.timer }),
  { start, stop, reset }
)(Actions)
