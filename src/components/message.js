import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

const Message = ({ timer }) => {
  return (
    <p className="app-message">
      {timer.isActive
        ? timer.isWorkTime
          ? 'Time to work !'
          : 'Time to break!'
        : ''}
    </p>
  )
}

Message.propTypes = {
  timer: PropTypes.object.isRequired
}

export default connect((state) => ({
  timer: state.timer
}))(Message)
