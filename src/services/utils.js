export const convertSecToTime = (seconds) =>
  new Date(seconds * 1000).toISOString().substr(11, 8)

export const convertMinToSec = (mins) => Math.floor(mins * 60)
export const convertSecToMin = (sec) => Math.floor(sec / 60)
