import {
  TIMER_START,
  TIMER_STOP,
  TIMER_TICK,
  TIMER_RESET,
  TIMER_START_BREAK,
  TIMER_START_WORK,
  TIMER_SET_COMPLETED_INTERVAL,
  CHANGE_SETTINGS,
  FETCH_SETTINGS
} from '../constants'

const initialState = {
  settings: {
    workInterval: 0,
    shortBreakInterval: 0,
    longBreakInterval: 0,
    longBreakAfter: 0
  },
  timer: {
    time: null,
    isActive: false,
    isWorkTime: true,
    completedIntervals: 0
  }
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case TIMER_START:
      return {
        ...state,
        timer: {
          ...state.timer,
          isActive: true,
          isFinished: false
        }
      }

    case TIMER_START_BREAK:
      return {
        ...state,
        timer: {
          ...state.timer,
          isWorkTime: false,
          time:
            state.timer.completedIntervals ===
            parseInt(state.settings.longBreakAfter)
              ? state.settings.longBreakInterval
              : state.settings.shortBreakInterval
        }
      }

    case TIMER_START_WORK:
      return {
        ...state,
        timer: {
          ...state.timer,
          isWorkTime: true,
          time: state.settings.workInterval
        }
      }

    case TIMER_SET_COMPLETED_INTERVAL:
      return {
        ...state,
        timer: {
          ...state.timer,
          completedIntervals: state.timer.completedIntervals + 1
        }
      }

    case TIMER_STOP:
      return {
        ...state,
        timer: {
          ...state.timer,
          isActive: false
        }
      }

    case TIMER_TICK:
      return {
        ...state,
        timer: {
          ...state.timer,
          time: state.timer.time - 1
        }
      }

    case TIMER_RESET:
      return {
        ...state,
        timer: {
          ...state.timer,
          isActive: false,
          time: state.settings.workInterval,
          completedIntervals: 0
        }
      }

    case FETCH_SETTINGS:
      return {
        ...state,
        timer: {
          ...state.timer,
          time: payload.workInterval
        },
        settings: {
          ...state.settings,
          ...payload
        }
      }

    case CHANGE_SETTINGS:
      if (state.timer.isActive) {
        return {
          ...state,
          settings: {
            ...state.settings,
            ...payload
          }
        }
      } else {
        return {
          ...state,
          timer: {
            ...state.timer,
            time: payload.workInterval ? payload.workInterval : state.timer.time
          },
          settings: {
            ...state.settings,
            ...payload
          }
        }
      }

    default:
      return state
  }
}
