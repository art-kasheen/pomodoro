import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { changeSettings, fetchSettings } from '../ac/index'
import { convertMinToSec, convertSecToMin } from '../services/utils'

const Settings = ({ settings, changeSettings, fetchSettings }) => {
  const handleChange = (ev) => {
    changeSettings({ [ev.target.name]: ev.target.value })
  }

  useEffect(() => {
    fetchSettings()
  }, [])

  return (
    <div className="app-settings">
      <div className="app-setting">
        <label htmlFor="workInterval">Work interval</label>
        <input
          id="workInterval"
          name="workInterval"
          type="range"
          min={convertMinToSec(1)}
          max={convertMinToSec(60)}
          step={convertMinToSec(1)}
          value={settings.workInterval}
          onChange={handleChange}
        />
        <span>{convertSecToMin(settings.workInterval)} minutes</span>
      </div>

      <div className="app-setting">
        <label htmlFor="shortBreakInterval">Short break interval</label>
        <input
          id="shortBreakInterval"
          name="shortBreakInterval"
          type="range"
          min={convertMinToSec(1)}
          max={convertMinToSec(60)}
          step={convertMinToSec(1)}
          value={settings.shortBreakInterval}
          onChange={handleChange}
        />
        <span>{convertSecToMin(settings.shortBreakInterval)} minutes</span>
      </div>

      <div className="app-setting">
        <label htmlFor="longBreakInterval">Long break interval</label>
        <input
          id="longBreakInterval"
          name="longBreakInterval"
          type="range"
          min={convertMinToSec(1)}
          max={convertMinToSec(60)}
          step={convertMinToSec(1)}
          value={settings.longBreakInterval}
          onChange={handleChange}
        />
        <span>{convertSecToMin(settings.longBreakInterval)} minutes</span>
      </div>

      <div className="app-setting">
        <label htmlFor="longBreakAfter">Long break after</label>
        <input
          id="longBreakAfter"
          name="longBreakAfter"
          type="range"
          min="1"
          max="10"
          step="1"
          value={settings.longBreakAfter}
          onChange={handleChange}
        />
        <span>{settings.longBreakAfter} intervals</span>
      </div>
    </div>
  )
}

Settings.propTypes = {
  settings: PropTypes.object.isRequired,
  changeSettings: PropTypes.func.isRequired,
  fetchSettings: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    settings: state.settings
  }),
  { changeSettings, fetchSettings }
)(Settings)
