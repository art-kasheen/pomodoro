import {
  FETCH_SETTINGS,
  CHANGE_SETTINGS,
  TIMER_START,
  TIMER_STOP,
  TIMER_TICK,
  TIMER_RESET,
  TIMER_START_BREAK,
  TIMER_START_WORK,
  TIMER_SET_COMPLETED_INTERVAL
} from '../constants'

export function start() {
  return {
    type: TIMER_START
  }
}

export function stop() {
  return {
    type: TIMER_STOP
  }
}

export function reset() {
  return {
    type: TIMER_RESET
  }
}

export function tick() {
  return {
    type: TIMER_TICK
  }
}

export function startBreak() {
  return {
    type: TIMER_START_BREAK
  }
}

export function startWork() {
  return {
    type: TIMER_START_WORK
  }
}

export function setCompletedInterval() {
  return {
    type: TIMER_SET_COMPLETED_INTERVAL
  }
}

export function fetchSettings() {
  return (dispatch) => {
    const settings = JSON.parse(localStorage.getItem('pomodoro'))

    if (settings) {
      dispatch({
        type: FETCH_SETTINGS,
        payload: settings
      })
    }
  }
}

export function changeSettings(settings) {
  return (dispatch, getState) => {
    const { settings: storeSettings } = getState()
    localStorage.setItem(
      'pomodoro',
      JSON.stringify({
        ...storeSettings,
        ...settings
      })
    )

    dispatch({
      type: CHANGE_SETTINGS,
      payload: settings
    })
  }
}
