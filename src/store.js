import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import reducer from './reducer'

const middlewares = applyMiddleware(thunk, logger)
const store = createStore(reducer, middlewares)

window.store = store

export default store
