import React, { useState, useEffect } from 'react'
import { usePrevious } from '../hooks/use-previous'
import { connect } from 'react-redux'
import { tick, startBreak, startWork, setCompletedInterval } from '../ac'
import PropTypes from 'prop-types'
import { convertSecToTime } from '../services/utils'

const Timer = ({
  timer,
  tick,
  startBreak,
  startWork,
  setCompletedInterval
}) => {
  const [timerId, setTimerId] = useState(null)
  const prevTimer = usePrevious(timer.isActive)
  const audio = new Audio(
    'https://s3.amazonaws.com/freecodecamp/simonSound1.mp3'
  )

  useEffect(() => {
    timer.isActive && !prevTimer && setTimerId(setInterval(tick, 1000))

    !timer.isActive && prevTimer && clearInterval(timerId)

    return () => clearInterval(timerId)
  }, [timer.isActive])

  useEffect(() => {
    timer.time === 0 &&
      timer.isWorkTime &&
      (setCompletedInterval() && startBreak() && audio.play())

    timer.time === 0 && !timer.isWorkTime && (startWork() && audio.play())
  }, [timer.time])

  return <div className="app-timer">{convertSecToTime(timer.time)}</div>
}

Timer.propTypes = {
  timer: PropTypes.object.isRequired,
  tick: PropTypes.func.isRequired,
  startBreak: PropTypes.func.isRequired,
  startWork: PropTypes.func.isRequired,
  setCompletedInterval: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    timer: state.timer
  }),
  { tick, startBreak, startWork, setCompletedInterval }
)(Timer)
