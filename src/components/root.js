import React from 'react'
import Header from './header'
import Timer from './timer'
import Actions from './actions'
import Settings from './settings'
import Message from './message'

export default () => {
  return (
    <div className="app">
      <div className="container">
        <Header />
        <Timer />
        <Message />
        <Actions />
        <Settings />
      </div>
    </div>
  )
}
